# 8085-opcodes

8085 instruction table (including undocumented) in Zilog mnemonics.

Converted from the Z80 instruction tables on the [CLR Home](https://clrhome.org/table/) web site.

Clone and view locally.

<div>
<table style="border: 2px solid #cccccc;">
<tbody>
<tr>
<td style="border: 1px solid #cccccc; padding: 6px;"><a href="https://gitlab.com/feilipu/8085-opcodes/-/raw/main/example.png" target="_blank"><img src="https://gitlab.com/feilipu/8085-opcodes/-/raw/main/example.png"/></a></td>
</tr>
<tr>
<th style="border: 1px solid #cccccc; padding: 6px;"><centre>Example Screenshot</center></th>
</tr>
</tbody>
</table>
</div>
